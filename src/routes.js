import React from "react";

import { Icon } from "@chakra-ui/react";
import {
  MdDashboard,
  MdHome,
  MdLock,
  MdOutlineShoppingCart,
} from "react-icons/md";

const routes = [];

export default routes;
