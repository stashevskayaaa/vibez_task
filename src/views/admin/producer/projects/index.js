/*!
  _   _  ___  ____  ___ ________  _   _   _   _ ___   ____  ____   ___  
 | | | |/ _ \|  _ \|_ _|__  / _ \| \ | | | | | |_ _| |  _ \|  _ \ / _ \ 
 | |_| | | | | |_) || |  / / | | |  \| | | | | || |  | |_) | |_) | | | |
 |  _  | |_| |  _ < | | / /| |_| | |\  | | |_| || |  |  __/|  _ <| |_| |
 |_| |_|\___/|_| \_\___/____\___/|_| \_|  \___/|___| |_|   |_| \_\\___/ 
                                                                                                                                                                                                                                                                                                                                       
=========================================================
* Horizon UI Dashboard PRO - v1.0.0
=========================================================

* Product Page: https://www.horizon-ui.com/pro/
* Copyright 2022 Horizon UI (https://www.horizon-ui.com/)

* Designed and Coded by Simmmple

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { useAuthContext } from "../../../../hooks/useAuthContext";
import { useProjectContext } from "../../../../hooks/useProjectContext";

// Chakra imports
import {
  Box,
  Text,
  Flex,
  useColorModeValue,
  SimpleGrid,
  Select,
} from "@chakra-ui/react";

// Custom components
import ProjectCard from "./components/ProjectCard";
import NoProjectView from "./components/NoProject";
import Template from "layouts/producer/Default";
import { Spinner } from "components/spinner";

export default function ProjectList() {
  const [projects, setProjects] = useState(null);
  const textColor = useColorModeValue("secondaryGray.900", "white");
  const buttonBg = useColorModeValue("transparent", "navy.800");
  const hoverButton = useColorModeValue(
    { bg: "gray.100" },
    { bg: "whiteAlpha.100" }
  );
  const activeButton = useColorModeValue(
    { bg: "gray.200" },
    { bg: "whiteAlpha.200" }
  );
  const [inProgress, setInProgress] = useState(false);
  const { user } = useAuthContext();
  const { dispatch } = useProjectContext();

  useEffect(() => {
    const fetchProjects = async () => {
      if (inProgress) return;
      if (!user) return;
      setInProgress(true);
      const response = await fetch(
        process.env.REACT_APP_API_URL + "/projects",
        {
          method: "GET",
          headers: {
            "Access-Control-Allow-Credentials": true,
            "Content-Type": "application/json",
            Authorization: user,
          },
        }
      );
      const json = await response.json();
      setInProgress(false);
      if (response.ok) setProjects(json.projects);
      localStorage.removeItem("project_id");
      dispatch({ type: "LOGOUT" });
    };

    if (user) {
      fetchProjects();
    }
  }, [user]);

  // Chakra Color Mode
  return (
    <Template>
      <NoProjectView projects={projects} />
      {inProgress && <Spinner/>}
      {projects?.length > 0 && (
        <SimpleGrid
          columns={{ base: 1, md: 2, xl: 4 }}
          gap="20px"
          paddingLeft={"40px"}
        >
          {projects.map((project, key) => (
            <Link to={`/projects/${project.id}`} key={key}>
              <ProjectCard project={project} />
            </Link>
          ))}
        </SimpleGrid>
      )}
    </Template>
  );
}
