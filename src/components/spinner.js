import { Flex } from "@chakra-ui/react";
import { BeatLoader } from "react-spinners";
//www.davidhu.io/react-spinners/storybook/?path=/docs/beatloader--main

export function Spinner() {
  // Chakra Color Mode
  return (
    <Flex w="100%" justifyContent={"space-around"} my="20px">
      <BeatLoader color="navy" size={10} />
    </Flex>
  );
}
